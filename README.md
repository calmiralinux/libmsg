# libmsg

`libmsg` is library for printing text messages to `stdout`/`stderr`/`stdin` and create pseudographic console windows (by `cursive` crate) written in Rust for Calmira GNU/Linux-libre project.

## Contents

[TOC]

## Usage

### Add

Write to the `Cargo.toml`:

```toml
...

[dependencies]
libmsg = { git = "https://gitlab.com/calmiralinux/libmsg" }
```

### Text user interfaces

```rust
use libmsg::Message;

fn main() {
  let mut msg = Message::new(0, "example");

  msg.error("This is error message!"); // "[!] [example] This is error message!"

  msg.set_indent(2);
  msg.msg("Simple message text"); // "  ==> [example] Simple message text"

  msg.del_oper();
  msg.msg("Simple message text"); // "  ==> Simple message text"

  msg.set_indent(4);
  msg.msg2("Message text (2-level)"); // "    --> Message text (2-level)"

  msg.set_indent(2);
  msg.info("Info message"); // "  [i] Info message"
}
```
