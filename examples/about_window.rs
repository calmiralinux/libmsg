use libmsg::tui::about::AboutWindowBuilder;
use libmsg::tui::about::Copyright;
use libmsg::tui::about::Donation;
use libmsg::tui::Tui;

use cursive::views::Dialog;
use cursive::event::Key;

fn about_win() -> Dialog {
	let description = format!(
		"{}\nPress <F1> key to open about `libmsg` crate window...",
		env!("CARGO_PKG_DESCRIPTION")
	);

	Dialog::around(
		AboutWindowBuilder::new(env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION"))
			.set_site("https://gitlab.com/calmiralinux/cabs/cport")
			.set_description(&description)
			.add_donation(Donation::new("Michail Krasnov", "Sberbank", "2222 2222 2222 2222"))
			.add_donation(Donation::new("Sergey Gaberer", "Unknown bank", "3333 3333 3333 3333"))
			.add_copyright(Copyright::new("2021-2023", "Michail Krasnov", "unknown@unknown.org"))
			.add_copyright(Copyright::new("2022-2023", "Sergey Gaberer", "unknown@unknown.com"))
			.build()
			.window()
	).title("About").button("OK", |s| s.quit())
}

fn main() {
	let win = about_win();
	let mut scr = Tui::new(libmsg::tui::Release::Testing).init();

	scr.cursive.add_layer(win);
	scr.cursive.add_global_callback(Key::F1, Tui::show_about_window);

	scr.cursive.run();
}
