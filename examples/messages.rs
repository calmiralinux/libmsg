use libmsg::set_colors;
use libmsg::Message;

fn main() {
    set_colors(true);
    let mut msg = Message::new(0, "no_open");
    msg.error(format!("indent: {}", msg.indent), false);
    msg.msg2("test_msg");
    msg.set_indent(4);
    msg.error(format!("indent: {}", msg.indent), false);
    msg.set_indent(2);
    msg.set_oper("cport");
    msg.error(format!("indent: {}", msg.indent), false);
    msg.info("info!!!");
    msg.ok("OK!");
    msg.warn("???");
    msg.set_indent(5);
    msg.del_oper();
    msg.error(format!("indent: {}", msg.indent), false);
    msg.msg("Hello, world!");
    if msg.dialog("Continue?", true).unwrap() {
        msg.msg2("OK");
    } else {
        msg.msg2("FAIL");
    }
}
