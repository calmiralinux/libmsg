use libmsg::Message;
use clap::Parser;
use clap::Subcommand;

#[derive(Parser)]
#[command(author, version, long_about = None)]
#[command(about = "Program for print text messages to stdout/stderr for cport")]
struct Cli {
    #[command(subcommand)]
    commands: Commands,
}

#[derive(Subcommand)]
enum Commands {
    Error {
        message: String,
    },
    Info {
        message: String,
    },
    Msg {
        message: String,
    },
    Msg2 {
        message: String,
    },
    Warn {
        message: String,
    },
    Ok {
        message: String,
    },
}

fn main() {
    let cli = Cli::parse();
    let mut msg = Message::default();
    msg.set_oper("shell");

    match cli.commands {
        Commands::Error { message } => {
            msg.error(message, false);
        },
        Commands::Info { message } => {
            msg.info(message);
        },
        Commands::Msg { message } => {
            msg.msg(message);
        },
        Commands::Msg2 { message } => {
            msg.msg2(message);
            msg.set_indent(0);
        },
        Commands::Warn { message } => {
            msg.warn(message);
        },
        Commands::Ok { message } => {
            msg.ok(message);
        }
    }
}
