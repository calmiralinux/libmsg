//! Show a statusbar line at the bottom of the terminal
//!
//! From [`cursive`](https://github.com/gyscos/cursive/blob/main/cursive/examples/status.rs)

use cursive::traits::Nameable as _;
use cursive::traits::Resizable as _;
use cursive::utils::markup::StyledString;
use cursive::view::View as _;

use cursive::views::FixedLayout;
use cursive::views::Layer;
use cursive::views::OnLayoutView;
use cursive::views::ResizedView;
use cursive::views::TextView;

use cursive::Rect;
use cursive::Vec2;

pub fn status_bar(status: StyledString) -> ResizedView<OnLayoutView<FixedLayout>> {
	let status = OnLayoutView::new(
		FixedLayout::new().child(
			Rect::from_point(Vec2::zero()),
			Layer::new(
				TextView::new(status)
					.with_name("status")
					.full_width()
			),
		),
		|layout, size| {
			layout.set_child_position(
				0, Rect::from_size(
					(0, size.y - 1), (size.x, 1)
				)
			);
			layout.layout(size);
		}
	);
	status.full_screen()
}
