use cursive::views::Panel;
use cursive::views::TextView;
use cursive::views::LinearLayout;
use cursive::traits::Scrollable;

use cursive::utils::markup::StyledString;
use cursive::theme::Effect;

pub struct Error {
	text: String,
	traceback: Option<String>,
}

pub struct ErrorBuilder {
	text: String,
	traceback: Option<String>,
}

impl ErrorBuilder {
	pub fn new(error_text: &str) -> Self {
		Self {
			text: error_text.to_string(),
			traceback: None,
		}
	}

	pub fn set_traceback_text(mut self, text: &str) -> Self {
		self.traceback = Some(text.to_string());
		self
	}

	pub fn build(self) -> Error {
		Error {
			text: self.text,
			traceback: self.traceback,
		}
	}
}

impl Error {
	pub fn window(&mut self) -> LinearLayout {
		let mut header_text = StyledString::new();
		header_text.append_styled("Error: ", Effect::Bold);
		header_text.append_plain(&self.text);

		let mut window_layout = LinearLayout::vertical()
			.child(TextView::new(header_text));

		if let Some(traceback) = &self.traceback {
			window_layout.add_child(
				Panel::new(TextView::new(traceback).scrollable())
					.title("Full traceback")
			);
		}

		window_layout
	}
}
