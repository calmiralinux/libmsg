//! Print information about program
//!
//! ## Example
//!
//! ```
//! use libmsg::tui::about::AboutWindowBuilder;
//! use libmsg::tui::about::Copyright;
//! use libmsg::tui::about::Donation;
//! use libmsg::tui::init;
//! use cursive::views::Dialog;
//!
//! let window = Dialog::around(
//!     AboutWindowBuilder::new("test_program", "1.0")
//!         .set_description("A simple example of using the libmsg library")
//!         .add_copyright(Copyright::new("2023", "Michail Krasnov", "https://gitlab.com/cov_id111"))
//!         .build()
//!         .window()
//! ).title("About this example").button("OK", |s| s.quit());
//!
//! let mut scr = init();
//! scr.add_layer(window);
//! scr.run();
//! ```

use cursive::utils::markup::StyledString;
use cursive::traits::Scrollable;
use cursive::theme::Effect;
use cursive::With;

use cursive::views::LinearLayout;
use cursive::views::ScrollView;
use cursive::views::TextView;
use cursive::views::Panel;

pub struct AboutWindow {
	pkg_name: String,
	pkg_version: String,
	pkg_description: Option<String>,
	site: Option<String>,
	copyright: Option<Vec<Copyright>>,
	donation: Option<Vec<Donation>>,
}

pub struct AboutWindowBuilder {
	pkg_name: String,
	pkg_version: String,
	pkg_description: Option<String>,
	site: Option<String>,
	copyright: Option<Vec<Copyright>>,
	donation: Option<Vec<Donation>>,
}

#[derive(Clone)]
pub struct Copyright {
	years: String,
	author: String,
	email: String,
}

#[derive(Clone)]
pub struct Donation {
	name: String,
	bank: String,
	card: String,
}

impl Copyright {
	pub fn new(years: &str, author: &str, email: &str) -> Self {
		Self {
			years: years.to_string(),
			author: author.to_string(),
			email: email.to_string(),
		}
	}

	pub fn format(&self) -> String {
		format!("Copyright (C) {} {} <{}>", &self.years, &self.author, &self.email)
	}
}

impl Donation {
	pub fn new(name: &str, bank: &str, card: &str) -> Self {
		Self {
			name: name.to_string(),
			bank: bank.to_string(),
			card: card.to_string(),
		}
	}
}

impl AboutWindowBuilder {
	pub fn new(name: &str, ver: &str) -> Self {
		Self {
			pkg_name: name.to_string(),
			pkg_version: ver.to_string(),
			pkg_description: None,
			site: None,
			copyright: None,
			donation: None,
		}
	}

	pub fn set_name(mut self, name: &str) -> Self {
		self.pkg_name = name.to_string();
		self
	}

	pub fn set_version(mut self, ver: &str) -> Self {
		self.pkg_version = ver.to_string();
		self
	}

	pub fn set_description(mut self, descr: &str) -> Self {
		self.pkg_description = Some(descr.to_string());
		self
	}

	pub fn set_site(mut self, site: &str) -> Self {
		self.site = Some(site.to_string());
		self
	}

	pub fn add_donation(mut self, donation: Donation) -> Self {
		match &mut self.donation {
			Some(don) => don.push(donation),
			None => self.donation = Some(vec![donation]),
		}
		self
	}

	pub fn add_copyright(mut self, copyright: Copyright) -> Self {
		match &mut self.copyright {
			Some(copy) => copy.push(copyright),
			None => self.copyright = Some(vec![copyright]),
		}
		self
	}

	pub fn build(self) -> AboutWindow {
		AboutWindow {
			pkg_name: self.pkg_name,
			pkg_version: self.pkg_version,
			pkg_description: self.pkg_description,
			site: self.site,
			copyright: self.copyright,
			donation: self.donation,
		}
	}
}

impl AboutWindow {
	pub fn window(&mut self) -> ScrollView<LinearLayout> {
		let mut main_text = StyledString::new();
		main_text.append_styled(&self.pkg_name, Effect::Bold);
		main_text.append_plain(" version ");
		main_text.append_styled(&self.pkg_version, Effect::Bold);

		if let Some(descr) = &self.pkg_description {
			main_text.append_plain(format!("\n\n{}", descr));
		}

		LinearLayout::vertical()
			.child(Panel::new(
				TextView::new(main_text)
			))
			.with(|lay| {
				if let Some(site) = &self.site {
					lay.add_child(
						Panel::new(TextView::new(site))
							.title("Site/repository of this project")
					);
				}

				if let Some(copyright) = &self.copyright {
					let mut copyright_layout = LinearLayout::vertical();
					for copy in copyright {
						copyright_layout.add_child(TextView::new(copy.format()));
					}
					lay.add_child(
						Panel::new(copyright_layout)
							.title("Authors")
					);
				}

				if let Some(donations) = &self.donation {
					let mut donation_layout = LinearLayout::vertical();
					for donation in donations {
						let mut donation_text = StyledString::styled(
							&donation.bank, Effect::Bold
						);
						donation_text.append_plain(
							format!(": {}", &donation.card)
						);

						donation_layout.add_child(
							Panel::new(TextView::new(donation_text))
								.title(format!("Donate: {}", &donation.name))
						);
					}
					lay.add_child(donation_layout);
				}
			}).scrollable()
	}
}
