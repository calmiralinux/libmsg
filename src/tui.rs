//! Building a pseudo-graphical terminal user interface

use cursive::Cursive;
use cursive::utils::markup::StyledString;
use cursive::CursiveRunnable;
use cursive::With;

use cursive::theme::BaseColor;
use cursive::theme::PaletteColor;
use cursive::theme::Color;
use cursive::theme::Effect;
use cursive::theme::Style;
use cursive::views::Dialog;

use self::about::AboutWindowBuilder;
use self::about::Copyright;

pub mod status_bar;
pub mod error;
pub mod about;

/// Type of program release where `libmsg` is used
///
/// When using a test version of the program, `libmsg`
/// windows will signal the user to do so
#[derive(Debug, PartialEq)]
pub enum Release {
	Stable,
	Testing,
}

impl Release {
	/// Method for detecting the type of release (stable or testing)
	///
	/// If the version number contains the characters `a`, `b`, or `rc`, the version
	/// is considered a test version. In all other cases, the program is considered
	/// stable.
	///
	/// > **Warning:** `libmsg` is designed for use in Calmira GNU/Linux-libre
	/// > projects, where the sequences `a`, `b`, `rc` are used to indicate development
	/// > stages (Alpha, Beta, RC). Other characters are not allowed in version numbers.
	pub fn detect_release_type(release: &str) -> Self {
		if release.contains('a') || release.contains('b') || release.contains("rc") {
			Self::Testing
		} else {
			Self::Stable
		}
	}
}

impl Default for Release {
	fn default() -> Self {
		Self::Stable
	}
}

/// Central part of the `libmsg` library.
pub struct Tui {
	release_type: Release,
	pub cursive: CursiveRunnable,
}

impl Tui {
	/// Creates a new Cursive root using one of the enabled backends.
	pub fn new(release_type: Release) -> Self {
		Self {
			cursive: cursive::default(),
			release_type,
		}
	}

	/// Specifies Cursive basic settings and performs initialization
	pub fn init(mut self) -> Self {
		let theme = self.cursive
			.current_theme()
			.clone()
			.with(|theme| {
				theme.palette[PaletteColor::Background] = match self.release_type {
					Release::Stable => Color::Dark(BaseColor::Black),
					Release::Testing => Color::Dark(BaseColor::Red),
				};
				theme.palette[PaletteColor::HighlightText] = Color::Light(
					BaseColor::White
				);
				theme.palette[PaletteColor::Highlight] = Color::Dark(BaseColor::Blue);
				theme.palette[PaletteColor::TitlePrimary] = Color::Dark(BaseColor::Blue);
		});

		self.cursive.pop_layer();
		self.cursive.set_theme(theme);

		if let Release::Testing = self.release_type {
			let mut note = StyledString::styled(
				"NOTE: ", Style::from(Effect::Bold)
			);
			note.append_plain(
				"this is UNSTABLE version of program! Press <",
			);
			note.append_styled("~", Style::from(Effect::Bold));
			note.append_plain("> to open debug console...");

			self.cursive.add_global_callback(
				'~', Cursive::toggle_debug_console
			);
			self.cursive.screen_mut().add_transparent_layer(
				status_bar::status_bar(note),
			);
		}
		self
	}

	pub fn show_about_window(scr: &mut Cursive) {
		let mut about = AboutWindowBuilder::new(env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION"))
			.set_site("https://gitlab.com/calmiralinux/cabs/cport/-/tree/v2.0/libmsg")
			.add_copyright(Copyright::new(
				"2023", "Calmira GNU/Linux-libre developers",
				"https://gitlab.com/calmiralinux/"
			))
			.build();

		scr.add_layer(Dialog::around(about.window())
			.title("About libmsg crate")
			.button("OK", |s| { s.pop_layer(); }));
	}
}

impl Default for Tui {
	fn default() -> Self {
		Self {
			release_type: Release::default(),
			cursive: cursive::default(),
		}
	}
}

pub fn init() -> CursiveRunnable {
	let tui = Tui::default().init();
	tui.cursive
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn detect_release_type_test() {
		let v1 = "2.0";
		let v2 = "2.0a1";
		let v3 = "2.0b3";
		let v4 = "2.0rc6";
		let v5 = "2.0_5g";

		assert_eq!(Release::detect_release_type(v1), Release::Stable);
		assert_eq!(Release::detect_release_type(v2), Release::Testing);
		assert_eq!(Release::detect_release_type(v3), Release::Testing);
		assert_eq!(Release::detect_release_type(v4), Release::Testing);
		assert_eq!(Release::detect_release_type(v5), Release::Stable);
	}
}
