use colored::Colorize;
use std::fmt::Display;
use std::io::stdin;
use std::io::stdout;
use std::io::Write;
use std::process;

pub mod tui;

pub struct Message {
    /// Name of the operation or program that sends the message to the terminal
    oper_name: Option<String>,

    /// Exit code when an error occurs (0-255)
    exit_code: u8,

    /// Line indentation in the terminal
    pub indent: usize,
}

enum Out {
    StdErr,
    StdOut,
}

impl Out {
    pub fn print<M: Display>(&self, msg: M) {
        match self {
            Self::StdErr => eprint!("{msg}"),
            Self::StdOut => print!("{msg}"),
        }
    }
}

pub fn set_colors(is_color: bool) {
    colored::control::set_override(is_color);
}

impl Message {
    pub fn new(indent: usize, oper_name: &str) -> Self {
        Self {
            oper_name: Some(oper_name.to_string()),
            exit_code: 1,
            indent,
        }
    }

    pub fn set_indent(&mut self, indent: usize) {
        self.indent = indent;
    }

    pub fn set_oper(&mut self, oper: &str) {
        self.oper_name = Some(oper.to_string());
    }

    pub fn set_exit_code(&mut self, code: u8) {
        self.exit_code = code;
    }

    pub fn del_oper(&mut self) {
        self.oper_name = None;
    }

    fn print_oper(&self, out: Out) {
        if self.oper_name.is_some() {
            if let Some(oper) = &self.oper_name {
                if !oper.is_empty() {
                    out.print(format!(
                        " {}{}{} ",
                        "[".dimmed(),
                        oper.dimmed(),
                        "]".dimmed()
                    ));
                } else {
                    out.print(" ");
                }
            }
        } else {
            out.print(" ");
        }
        stdout().flush().unwrap();
    }

    pub fn error<M: Display>(&self, msg: M, is_exit: bool) {
        eprint!(
            "{:>indent$}[{}]",
            "",
            "E".bold().bright_red(),
            indent = self.indent
        );
        self.print_oper(Out::StdErr);
        eprintln!("{}", msg);

        if is_exit {
            process::exit(self.exit_code.into());
        }
    }

    pub fn info<M: Display>(&self, msg: M) {
        print!(
            "{:>indent$}[{}]",
            "",
            "i".bold().bright_cyan(),
            indent = self.indent
        );
        self.print_oper(Out::StdOut);
        println!("{}", msg);
    }

    pub fn msg<M: Display>(&self, msg: M) {
        print!(
            "{:>indent$}{}",
            "",
            "==>".bold().yellow(),
            indent = self.indent
        );
        self.print_oper(Out::StdOut);
        println!("{}", msg);
    }

    pub fn msg2<M: Display>(&self, msg: M) {
        print!(
            "{:>indent$}{}",
            "",
            "-->".bold().blue(),
            indent = self.indent + 2
        );
        println!(" {}", msg);
    }

    pub fn warn<M: Display>(&self, msg: M) {
        print!(
            "{:>indent$}[{}]",
            "",
            "W".bold().on_yellow(),
            indent = self.indent
        );
        self.print_oper(Out::StdOut);
        println!("{}", msg);
    }

    pub fn ok<M: Display>(&self, msg: M) {
        print!(
            "{:>indent$}[{}]",
            "",
            "✓".bold().green(),
            indent = self.indent
        );
        self.print_oper(Out::StdOut);
        println!("{}", msg);
    }

    pub fn dialog<M: Display>(&self, msg: M, default_no: bool) -> Result<bool, String> {
        print!(
            "{:>indent$}{}",
            "",
            ":: ".bold().bright_magenta(),
            indent = self.indent
        );
        print!("{} ", msg);

        if default_no {
            print!("[y/{}] ", "N".bold());
        } else {
            print!("[{}/n] ", "Y".bold());
        }
        stdout().flush().map_err(|err| err.to_string())?;

        let mut enter = String::new();
        stdin()
            .read_line(&mut enter)
            .map_err(|err| err.to_string())?;

        let enter = enter.trim();

        if enter == "n" || enter == "N" {
            Ok(false)
        } else if enter == "y" || enter == "Y" {
            Ok(true)
        } else if enter.is_empty() {
            if default_no {
                Ok(false)
            } else {
                Ok(true)
            }
        } else {
            self.error("Wrong input!", false);
            self.dialog(msg, default_no)
        }
    }
}

impl Default for Message {
    fn default() -> Self {
        Self {
            oper_name: None,
            exit_code: 1,
            indent: 0,
        }
    }
}
